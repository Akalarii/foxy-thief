﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static bool facingLeft = true;
    protected Rigidbody2D enemyRb;
    public bool stationary;

    [Header("Movement:")]
    public float moveSpeed;
    public float leftCap;
    public float rightCap;
    public float delay;
    
    [Header("Reference:")] 
    public LayerMask playerLayer;
    public GameObject alertRadius;
    public float attackRange = 5f;
    

    protected void Movement()
    {
        if (facingLeft)
        {
            if (enemyRb.position.x >= leftCap)
            {
                enemyRb.velocity = new Vector2(-moveSpeed, 0);
            }
            else
            {
                enemyRb.transform.localScale = new Vector3(-1, 1, 1);
                StartCoroutine(WaitLeft());
            }
        }

        if (facingLeft == false)
        {
            if (enemyRb.position.x <= rightCap)
            {
                enemyRb.velocity = new Vector2(moveSpeed, 0);
            }

            if (enemyRb.position.x >= rightCap)
            {
                enemyRb.transform.localScale = new Vector3(1, 1, 1);
                StartCoroutine(WaitRight());
            }
        }
    }
    private IEnumerator WaitLeft()
    {
        enemyRb.velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(delay);
        facingLeft = false;
    }
    private IEnumerator WaitRight()
    {
        enemyRb.velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(delay);
        facingLeft = true;
    }
    
}
