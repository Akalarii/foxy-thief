﻿using System;
using System.Collections;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    private Enemy enemy;
    private Rigidbody2D playerRb;
    //-----
    public GameObject Player;
    private SpriteRenderer rend;
    private Vector2 origin;
    //-----
    private Collider2D playerColl;

    //-----
    public bool hiding = false;
    private GameObject hidingPlace;

    
        
    //----------------------------------------------------------------------------------------------------------------

    [Header("Combat: ")] 
    public LayerMask Ground;
    public LayerMask Enemy;
    public GameObject Crane;

    [Header("UI: ")]
    
    [Header("Range: ")] 
    public GameObject RedSquare;
    public float attackRange;

    //----------------------------------------------------------------------------------------------------------------

    private void Start()
    {
        playerRb = Player.GetComponent<Rigidbody2D>();
        playerColl = Player.GetComponent<Collider2D>();
        rend = GetComponentInParent<SpriteRenderer>();
    }
    private void Update()
    {
        combat();
        if (Input.GetKeyDown(KeyCode.H))
        {
            if (hiding)
            {
                //selfLight.SetActive(true);
                rend.enabled = true;
                playerRb.transform.position = origin;                                                                    //Resetting PlayerPosition to origin
                Player.layer = 10;                                                                                       //Making Player vulnerable again.
                hiding = false;
                Debug.Log("Leaving Hideout...");
            }
            else
            {
                if (hidingPlace != null)
                {
                    rend.enabled = false;
                    playerRb.transform.localPosition = 
                        new Vector3(hidingPlace.transform.localPosition.x, playerRb.transform.position.y, 0);         //Player disappears behind corresponding GameObject.
                    origin = playerRb.transform.position;                                                                //Defining origin position (will be used in stopHide())
                    Player.layer = 15;                                                                                    //Making Player invincible
                    hiding = true;
                    //selfLight.SetActive(false);
                    Debug.Log("Hiding!");
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("hideout"))
        {
            hidingPlace = other.gameObject;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        hidingPlace = null;
    }
    private void combat()
    {
        if (Input.GetKeyDown(KeyCode.F) && playerColl.IsTouchingLayers(Ground))
        {
            RedSquare.SetActive(true);
            StartCoroutine(WaitTill(0.1f));
            if (Physics2D.OverlapCircle(transform.position, attackRange, Enemy))
            {
                Debug.Log("Enemy hit!");
                Crane.layer = 12;
                Crane.SetActive(false);
                StartCoroutine(ExecuteAfterTime(10));
            }
            else
            {
                Debug.Log("miss!");
            }
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;                                                 //Making AttackRange visible in Unity_Editor
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }

    //----------------------------------------------------------------------------------------------------------------

    private IEnumerator WaitTill(float delay)
    {
        yield return new WaitForSeconds(delay);
        RedSquare.SetActive(false);
    }
    private IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        Crane.layer = 9;
        Debug.Log("Crane woke up again!");
    }
}
   