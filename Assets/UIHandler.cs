﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    public Transform[] checkpoints;
    public GameObject buttonObject;
    public RectTransform parent;
    public GameObject cheatScreen, pauseScreen;
    public float buttonHeight;
    private int buttonsSpawned;

// Start is called before the first frame update
    void Start()
    {
        foreach (Transform checkpoint in checkpoints)
        {
            GameObject instance = Instantiate(buttonObject, parent);
            instance.GetComponent<RectTransform>().anchoredPosition = Vector2.down * (buttonsSpawned * buttonHeight + 10);
            instance.GetComponentInChildren<TextMeshProUGUI>().text = checkpoint.gameObject.name;
            int id = buttonsSpawned;
            instance.GetComponent<Button>().onClick.AddListener(() => TpToCheckPoint(id));
            buttonsSpawned++;
        }
        parent.sizeDelta = new Vector2(0, buttonsSpawned * buttonHeight + 10);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            pauseScreen.SetActive(true);
        }
    }

    public void TpToCheckPoint(int id)
    {
        GameObject.FindWithTag("Player").transform.position = checkpoints[id].position;
        cheatScreen.SetActive(false);
    }

    public void ReloadScene(int id)
    {
        SceneManager.LoadScene(id);
    }
}
