﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundmanager : MonoBehaviour
{

    public static AudioClip alarm, birdAlarm, gameplayMusic, titleMusic, land, punch, collect, swing, checkpoint, damnit, lion;
    private static AudioSource audioSrc;


    private void Start()
    {
        alarm = Resources.Load<AudioClip>("alarm");
        birdAlarm = Resources.Load<AudioClip>("birdAlarm");
        gameplayMusic = Resources.Load<AudioClip>("gameplay2");
        titleMusic = Resources.Load<AudioClip>("titlescreen");
        land = Resources.Load<AudioClip>("landing");
        punch = Resources.Load<AudioClip>("Punch");
        collect = Resources.Load<AudioClip>("collect");
        swing = Resources.Load<AudioClip>("swing");
        checkpoint = Resources.Load<AudioClip>("checkpoint");
        damnit = Resources.Load<AudioClip>("death");
        lion = Resources.Load<AudioClip>("lion");

        audioSrc = GetComponent<AudioSource>();
    }
    


    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "alarm":
                audioSrc.PlayOneShot(alarm);
                break;
            case "birdAlarm":
                audioSrc.PlayOneShot(birdAlarm);                //check
                break;
            case "land":
                audioSrc.PlayOneShot(land);                     //check
                break;
            case "punch":
                audioSrc.PlayOneShot(punch);                    //check
                break;
            case "collect":
                audioSrc.PlayOneShot(collect);                  //check
                break;
            case "swing":
                audioSrc.PlayOneShot(swing);                    //check
                break;
            case "checkpoint":
                audioSrc.PlayOneShot(checkpoint);                //check
                break;
            case "death":
                audioSrc.PlayOneShot(damnit);                    //check
                break;
            case "lion":
                audioSrc.PlayOneShot(lion);                    //check
                break;
           
        }
    }
}
