﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

public class newLion : MonoBehaviour
{
    private Rigidbody2D enemyRb;
    private Animator anim;
    private Collider2D coll;

    private float alertedCD = 4;
    private float alertedReset = 0;
    private float turnCD = 1;
    private float turnReset = 0;

    private bool facingLeft = true;
    private bool alerted;

    public float moveSpeed;
    public float leftCap;
    public float rightCap;
    public float delay;
    public float attackRange = 5f;
    private float playerPosition;
    
    public bool stationary;

    private GameObject player;
    public GameObject FlashLight;
    public LayerMask playerLayer;
    public GameObject alertRadius;
    public Animator animEx;
    //----------------------------------------------------------------------------------------------------------------

    private void Start()
    {
        coll = GetComponent<Collider2D>();
        enemyRb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player");
    }
    private void Update()
    {
        //always...---------------------------------------------------------------------------------------------------
        Animation();

        //idling...----------------------------------------------------------------------------------------------------
        if (stationary == false)                //if Lion is set to moving...
        {
            if (alerted == false)               //if Lion is idling...
            {
                Movement();                     //then move normally.
            }
        }
        
        //triggering 'alerted'...---------------------------------------------------------------------------------------
        Collider2D alertCheck = Physics2D.OverlapCircle(alertRadius.transform.position, attackRange, playerLayer);

        if (alertCheck != null && Player.noisy)                  //if Player landed within radius of this Lion...
        {
            alerted = true;                                      //Lion is alerted!
        }
        
        //what 'alerted' does...----------------------------------------------------------------------------------------
        
        if (alerted)
        {
            animEx.SetBool("alerted", true);        //Show Exclamation Mark..
            turnReset += Time.deltaTime;                         //Start Countdown till first turn around..
            alertedReset += Time.deltaTime;                      //Start Countdown till alerted-state ends..
            
            if (turnReset >= turnCD)
            // This is what happens as soon as Lion turns around.
            {
                animEx.SetBool("alerted", false);                           //Hide Exclamation Mark.

                if (alertedReset >= alertedCD)                   
                // This is what happens as soon as alerted-state ends.
                {
                    alerted = false;
                    alertedReset = 0;
                    turnReset = 0;

                    if (facingLeft)
                    {
                        transform.localScale = new Vector3(1, 1, 1);
                    }
                    else
                    {
                        transform.localScale = new Vector3(-1, 1, 1);
                    }
                }
                turnReset = 0;
            }

            if (alertedReset >= turnCD)
                StartCoroutine(WalkTowardPlayer());
            
            
        }

        if (Player.safe == false)
        {
            StartCoroutine(Spotted());
            soundmanager.PlaySound("lionAlarm");
        }
        
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            Player.safe = false;
            soundmanager.PlaySound("lionAlarm");
        }
    }
    //---------------------------------------------------------------------------------------------------------------

    private void Animation()
    {
        if (enemyRb.velocity.x > 0)
        {
            anim.SetInteger("movementState", 1);
        }
        else if (enemyRb.velocity.x < 0)
        {
            anim.SetInteger("movementState", 1);
        }
        else
        {
            anim.SetInteger("movementState", 0);
        }
    }

    private void Movement()
    {
        if (facingLeft)
        {
            if (enemyRb.position.x >= leftCap)
            {
                enemyRb.velocity = new Vector2(-moveSpeed, 0);
            }
            else
            {
                StartCoroutine(WaitLeft());
            }
        }

        if (facingLeft == false)
        {
            if (enemyRb.position.x <= rightCap)
            {
                enemyRb.velocity = new Vector2(moveSpeed, 0);
            }

            if (enemyRb.position.x >= rightCap)
            {
                StartCoroutine(WaitRight());
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------

    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(alertRadius.transform.position, attackRange);
    }

    private IEnumerator WalkTowardPlayer()
    {
        if (player.transform.position.x > enemyRb.position.x)
        {
            enemyRb.transform.localScale = new Vector3(-1, 1, 1);
            enemyRb.velocity = new Vector2(moveSpeed * 2.5f, enemyRb.velocity.y);
            if(enemyRb.transform.position.x >= rightCap)
                enemyRb.velocity = new Vector2(0, 0);
        }

        if (player.transform.position.x < enemyRb.position.x)
        {
            enemyRb.transform.localScale = new Vector3(1, 1, 1);
            enemyRb.velocity = new Vector2(-moveSpeed * 2.5f, enemyRb.velocity.y);
            if(enemyRb.transform.position.x <= leftCap)
                enemyRb.velocity = new Vector2(0,0);
        }

        yield return new WaitForSeconds(alertedCD - turnCD);
    }
    
    private IEnumerator WaitLeft()
    {
        enemyRb.velocity = new Vector2(0, 0);
        anim.SetInteger("movementState", 0);
        yield return new WaitForSeconds(delay);
        enemyRb.transform.localScale = new Vector3(-1, 1, 1);
        facingLeft = false;
    }
    private IEnumerator WaitRight()
    {
        enemyRb.velocity = new Vector2(0, 0);
        anim.SetInteger("movementState", 0);
        yield return new WaitForSeconds(delay);
        facingLeft = true;
        enemyRb.transform.localScale = new Vector3(1, 1, 1);
    }
    private IEnumerator Spotted()
    {
        stationary = true;
        yield return new WaitForSeconds(Player.death);
        stationary = false;
    }

}