﻿using System.Collections;
using UnityEngine;


public class Crane : MonoBehaviour
{
    private Rigidbody2D enemyRb;
    private Animator anim;
    private Collider2D coll;
    private SpriteRenderer renderer;

    private float alertedCD = 4;
    private float alertedReset = 0;
    private float turnCD = 1;
    private float turnReset = 0;

    private bool facingLeft = true;
    private bool alerted;
    private bool unconscious = false;
    private bool fixated = false;

    public float moveSpeed;
    public float leftCap;
    public float rightCap;
    public float delay;
    public float unconsciousCD;
    public float attackRange = 5f;
    
    public bool stationary;
    public bool hasKey;

    public Sprite withKeyTexture;
    public Sprite withoutKeyTexture;

    public RuntimeAnimatorController withKeyAnimator;
    public RuntimeAnimatorController withoutKeyAnimator;
    
    private GameObject player;
    public GameObject FlashLight;
    public LayerMask playerLayer;
    public GameObject alertRadius;
    public Animator animEx;
    //----------------------------------------------------------------------------------------------------------------
    private void Start()
    {
        coll = GetComponent<Collider2D>();
        enemyRb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
        SetAnimator();
        SetRenderSprite();
        player = GameObject.FindWithTag("Player");
        
    }
    
    private void SetRenderSprite()
    {
        renderer.sprite = hasKey ? withKeyTexture : withoutKeyTexture;
    }

    private void SetAnimator()
    {
        anim.runtimeAnimatorController = hasKey ? withKeyAnimator : withoutKeyAnimator;
    }

    private void Update()
    {
        if (unconscious == false) 
        {
            if (stationary == false)
            {
                if (alerted == false)
                {
                    Movement();
                }
            }
            Collider2D alertCheck = Physics2D.OverlapCircle(alertRadius.transform.position, attackRange, playerLayer);

            if (alertCheck != null && Player.noisy)
            {
                alerted = true;
            }
        }
        Animation();
        
        if (alerted)
        {
            if(unconscious == false)
            {
                animEx.SetBool("alerted", true);
                turnReset += Time.deltaTime;
                alertedReset += Time.deltaTime;
                if (turnReset >= turnCD)
                {
                    Alert();
                    animEx.SetBool("alerted", false);

                    if (alertedReset >= alertedCD)
                    {
                        alerted = false;
                        fixated = false;
                        alertedReset = 0;
                        turnReset = 0;

                        if (facingLeft)
                        {
                            transform.localScale = new Vector3(1, 1, 1);
                        }
                        else
                        {
                            transform.localScale = new Vector3(-1, 1, 1);
                        }
                    }

                    turnReset = 0;
                }
            }
            else
            {
                LookAt();
            }
        }
        /*if (Player.safe == false)
        {
            stationary = true;
            LookAt();
        }*/
    }
    //---------------------------------------------------------------------------------------------------------------
    private void Alert()
    {
        if (alerted)
        {
            LookAt();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            Player.safe = false;
            StartCoroutine(Spotted());
            soundmanager.PlaySound("birdAlarm");
        }
    }

    private void LookAt()
    {
        if (fixated == false)
        {
            if (enemyRb.position.x > player.transform.position.x)
            {
                enemyRb.transform.localScale = new Vector3(1, 1, 1);
                fixated = true;
            }

            if (enemyRb.position.x < player.transform.position.x)
            {
                enemyRb.transform.localScale = new Vector3(-1, 1, 1);
                fixated = true;
            }
        }
    }

    private void Animation()
    {
        if (enemyRb.velocity.x > 0)
        {
            anim.SetInteger("movementState", 1);
        }
        else if (enemyRb.velocity.x < 0)
        {
            anim.SetInteger("movementState", 1);
        }
        else
        {
            anim.SetInteger("movementState", unconscious ? 2 : 0);
        }
    }

    public void Unconscious()
    {
        this.coll.enabled = false;
        unconscious = true;
        animEx.SetBool("alerted", false);
        StartCoroutine(TimeTillAwake());
        FlashLight.SetActive(false);
        Debug.Log("Unconscious!");
        anim.SetInteger("movementState", 2);
    }

    private void Movement()
    {
        if (facingLeft)
        {
            if (enemyRb.position.x >= leftCap)
            {
                enemyRb.velocity = new Vector2(-moveSpeed, 0);
            }
            else
            {
                StartCoroutine(WaitLeft());
            }
        }

        if (facingLeft == false)
        {
            if (enemyRb.position.x <= rightCap)
            {
                enemyRb.velocity = new Vector2(moveSpeed, 0);
            }

            if (enemyRb.position.x >= rightCap)
            {
                StartCoroutine(WaitRight());
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------

    private IEnumerator TimeTillAwake()
    {
        yield return new WaitForSeconds(unconsciousCD);
        coll.enabled = true;
        unconscious = false;
        FlashLight.SetActive(true);
        Debug.Log("not unconscious anymore.");
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(alertRadius.transform.position, attackRange);
    }
    
    private IEnumerator WaitLeft()
    {
        enemyRb.velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(delay);
        enemyRb.transform.localScale = new Vector3(-1, 1, 1);
        facingLeft = false;
    }
    private IEnumerator WaitRight()
    {
        enemyRb.velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(delay);
        facingLeft = true;
        enemyRb.transform.localScale = new Vector3(1, 1, 1);
    }

    private IEnumerator Spotted()
    {
        stationary = true;
        yield return new WaitForSeconds(Player.death);
        stationary = false;
    }

}