﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class laserbeam : MonoBehaviour
{
    private bool boot = true;
    private bool on;

    
    private SpriteRenderer rend;
    private Collider2D coll;
    
    public float InitialCD;
    private float initialCDReset = 0;
    
    
    public float onCD;
    private float onCDReset = 0;
    public float offCD;
    private float offCDReset = 0;


    private void Awake()
    {
        rend = GetComponent<SpriteRenderer>();
        coll = GetComponent<Collider2D>();
    }

    private void Start()
    {
        rend.enabled = false;
        coll.enabled = false;
    }

    private void Update()
    {
        if (boot)
        {
            initialCDReset += Time.deltaTime;

            if (initialCDReset >= InitialCD)
            {
                boot = false;
                initialCDReset = 0;
                rend.enabled = true;
                coll.enabled = true;
                on = true;
            }
        }

        else if (on)
        {
            onCDReset += Time.deltaTime;
            if (onCDReset >= onCD)
            {
                rend.enabled = false;
                coll.enabled = false;
                on = false;
                onCDReset = 0;
            }
        }
        else
        {
            offCDReset += Time.deltaTime;
            if (offCDReset >= offCD)
            {
                rend.enabled = true;
                coll.enabled = true;
                on = true;
                offCDReset = 0;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player.safe = false;
        }
    }
}
