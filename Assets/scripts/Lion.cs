﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lion : Enemy
{
    private Animator anim;
    public GameObject Player;
    public bool isAlerted = false;
    public float maxDistance = 5f;
    public float walkDuration;
    public float walkStart = 0f;

    private void Start()
    {
        enemyRb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (stationary == false)
        {
            if (isAlerted == false)
            {   
                Movement(); 
                Animation();
            }
        }

        if (isAlerted == true)
        {
            Walking();
        }
    }
    
    private void Animation()
    {
        if (enemyRb.velocity.x > Mathf.Abs(0))
        {
            anim.SetInteger("movementState", 1);
        }
        else if (enemyRb.velocity.x < Mathf.Abs(0))
        {
            anim.SetInteger("movementState", 1);
        }
        else
        {
            anim.SetInteger("movementState", 0);
        }
    }
    

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            if (PlayerController.alertState == 1 && isAlerted == false)
            {
                Debug.Log("Lion is alerted!");
                isAlerted = true;
                
                if (other.transform.position.x < this.transform.position.x)
                {
                    transform.localScale = new Vector3(1, 1, 1);
                    enemyRb.velocity = new Vector2(-moveSpeed - 1, 0);
                    if((this.transform.position.x - other.transform.position.x) < maxDistance)
                    {
                        enemyRb.velocity = new Vector2(0, 0);
                    }
                }
                if (other.transform.position.x > this.transform.position.x)
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                    enemyRb.velocity = new Vector2(moveSpeed + 1, 0);
                    if((other.transform.position.x -this.transform.position.x ) < maxDistance)
                    {
                        enemyRb.velocity = new Vector2(0, 0);
                    }
                }
                //StartCoroutine(alerted());
                //transform.position += transform.forward * moveSpeed * Time.deltaTime;
            }
        }
    }
    private void Walking()
    {
        walkStart += Time.deltaTime;
        if(walkStart > walkDuration)
        {
            isAlerted = false;
            walkStart = 0;
        }
    }
    /*private IEnumerator alerted()
    {
        yield return new WaitForSeconds(5);
        isAlerted = false;
    }*/
}
