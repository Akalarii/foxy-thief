﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashlightlion : MonoBehaviour
{

    public GameObject[] maxAngle;
    public float rayDistance;

    private newLion lion;


    private void Start()
    {
        lion = GetComponentInParent<newLion>();
    }

    private void Update()
    {
        Detection();
    }

    private void Detection()
    {
        var position = transform.position;

        foreach (var angle in maxAngle)
        {
            RaycastHit2D hitInfoLion =
                Physics2D.Raycast(position, (angle.transform.position - position), rayDistance, ~18);

            if (hitInfoLion && hitInfoLion.collider.gameObject.CompareTag("Player"))
            {
                StartCoroutine(Spotted());
                soundmanager.PlaySound("lion");
                Player.safe = false;
            }
        }
    }

    private IEnumerator Spotted()
    {
        lion.stationary = true;
        yield return new WaitForSeconds(Player.death);
        lion.stationary = false;
    }
}
    

