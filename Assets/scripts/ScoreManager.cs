﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{

    public static ScoreManager instance;
    public TextMeshProUGUI text;
    private int score;


    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void ChangeScore(int value)
    {
        score += value;
        UpdateScore();
    }
    
    public void UpdateScore()
    {
        text.text = String.Concat(score, " | 437 000 $");
    }
}
