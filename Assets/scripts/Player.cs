﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Components
    public CapsuleCollider2D coll;
    private Rigidbody2D rb;
    private Animator anim;
    private SpriteRenderer rend;
    
    //Other LayerMasks;
    public LayerMask Ground;
    public LayerMask Glass;
    public LayerMask Enemy;

    //Other GameObjects;
    public GameObject RedSquare;                            //can be removed after debugging.
    public GameObject camDefault;
    public GameObject camCrouch;
    public GameObject foxHitBox;
    public GameObject feetCollider;
    private GameObject hidingPlace;
    public GameObject keySprite;
    private GameMaster gm;
    
    //Vectors;
    private Vector2 origin;
    
    //Floats
    public float moveSpeed;
    public float jumpForce;
    public float jumpAirResistance;
    public float fallForce;
    public float hitRange;
    public static float death = 2;
    
    //CoolDowns
    public float hitCooldown;
    private float hitCooldownEnd = 1f;
    private float noisyCD = 0.5f;
    private float noisyCDStart = 0;
    public float crouchZoomCD = 1f;
    private float crouchCDStart = 0;
    private float jumpCD = 0.4f;
    private float jumpCDStart = 0;
    private float punchingCD = 0.3f;
    private float punchingCDReset = 0;
    
    //Booleans
    public static bool connected;
    public static bool keyState = false;
    public static bool noisy = false;
    public static bool safe = true;
    private bool jumping;
    private bool falling;
    private bool crouching = false;
    private bool cdAttack = false;
    private bool punching;

    [Header("Cheats")] 
    public bool invincible = false;

    private void Start()
    {
        coll = GetComponent<CapsuleCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        rend = GetComponent<SpriteRenderer>();
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
    }

    private void FixedUpdate()
    {
        Movement();
    }

    private void Update()
    {
        {
            Fall();
            Jump();
            Crouch();
            Death();
        }

        if (cdAttack == false)
        {
            Combat();
        }
        
        if (punching)
        {
            punchingCDReset += Time.deltaTime;
            if (punchingCDReset >= punchingCD)
            {
                punching = false;
                cdAttack = false;
                punchingCDReset = 0;
            }
        
            
            else
            {
                hitCooldown += Time.deltaTime;
                if (hitCooldown >= hitCooldownEnd)
                {
                    cdAttack = false;
                }
            }
        }

        if (noisy)
        {
            noisyCDStart += Time.deltaTime;
            if (noisyCDStart >= noisyCD)
            {
                noisy = false;
                noisyCDStart = 0;
            }
        }

        if (jumping)
        {
            jumpCDStart += Time.deltaTime;
            //anim.SetInteger("state", 4);
            if (jumpCDStart >= jumpCD && IsGrounded())
            {
                jumping = false;
                noisy = true;
                jumpCDStart = 0;
            }
        }

        if (crouching)
        {
            crouchCDStart += Time.deltaTime;
            if (crouchCDStart >= crouchZoomCD)
            {
                camCrouch.SetActive(true);
                camDefault.SetActive(false);
            }
        }
    }

    private bool IsGrounded()
    {
        float extraHeight = 0.3f;
        var bounds = coll.bounds;
        RaycastHit2D raycast = Physics2D.Raycast(feetCollider.transform.position, Vector2.down, extraHeight, Ground | Glass);
        Debug.DrawRay(feetCollider.transform.position, Vector2.down * (extraHeight), Color.red);
        return raycast.collider != null;
    }

    private void Death()
    {
        if (safe == false)
        {
            if(invincible == false)
            {
                anim.SetInteger("state", 7);
                StartCoroutine(TimeBeforeDeath());
            }
            else
            {
                safe = true;
            }
        }
    }
    private void Movement()
    {
        if (crouching == false)
        {
            //Movement
            
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
            transform.position += Time.deltaTime * moveSpeed * movement;

            if (!punching)
            {
                if (Mathf.Abs(movement.x) > 0)
                {
                    anim.SetInteger("state", 1);
                }
                else
                {
                    anim.SetInteger("state", 0);
                }
            }

            //Flip Sprite
            if (movement.x > 0) rb.transform.localScale = new Vector3(1, 1, 1); 
            if (movement.x < 0) rb.transform.localScale = new Vector3(-1, 1, 1);
        
            //AntiForce in air.
            if (rb.velocity.y >= 0)
            {
                if (rb.velocity.x >= 5) rb.AddForce(Time.deltaTime * jumpAirResistance * Vector2.left);
                if (rb.velocity.x <= -5) rb.AddForce(Time.deltaTime * jumpAirResistance * Vector2.right);
            }
        }
    }
    private void Jump()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded() && crouching == false)
        {
            rb.velocity = Vector2.up * jumpForce;
            jumping = true;
            anim.SetInteger("state", 4);
        }

        if (jumping)
        {
            //faster falling
            if (rb.velocity.y < 0) 
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (fallForce - 1) * Time.deltaTime;
            }

            //control jump height by length of time jump button held
            if (rb.velocity.y > 0 && !Input.GetButton("Jump")) 
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (fallForce - 1) * Time.deltaTime;
            }
        }
    }
    private void Fall()
    {
        if (rb.velocity.y < 0 && !IsGrounded())
        {
            falling = true;
            jumping = false;
        }

        if (falling)
        {
            anim.SetInteger("state", 5);
            if (IsGrounded())
            {
                anim.SetInteger("state", 0);
                soundmanager.PlaySound("land");
                noisy = true;
                falling = false;
            }
        }
    }
    private void Crouch()
    {
        if (Input.GetKey(KeyCode.S) && IsGrounded())
        {
            if (crouching == false)
            {
                //--CROUCH--
                anim.SetInteger("state",3);
                crouching = true;                                                    
                noisy = false;
                coll.size = new Vector2(0.45f, 0.7f);                                          
                coll.offset = new Vector2(0.05f, -0.5f);
            }
        }
        else
        {
            crouching = false;
            coll.size = new Vector2(0.45f, 1.7f);
            coll.offset = new Vector2(0.05f, 0);

            crouchCDStart = 0;
            camDefault.SetActive(true);
            camCrouch.SetActive(false);
        }
    }
    private void Combat()
    {
        if (Input.GetKeyDown(KeyCode.F) && IsGrounded())
        {
            punching = true;
            if (punching)
            {
                anim.SetInteger("state", 6);
                cdAttack = true;
                hitCooldown = 0;
                Collider2D enemyHit = Physics2D.OverlapCircle(foxHitBox.transform.position, hitRange, Enemy);

                if (enemyHit)
                {
                    anim.SetInteger("state", 6);
                    enemyHit.GetComponent<Crane>().Unconscious();
                    soundmanager.PlaySound("punch");
                    if (enemyHit.GetComponent<Crane>().hasKey)
                    {
                        keySprite.SetActive(true);
                        keyState = true;
                    }
                }
                else
                {
                    soundmanager.PlaySound("swing");
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(foxHitBox.transform.position, hitRange);
    }
    private IEnumerator TimeBeforeDeath()
    {
        this.enabled = false;
        coll.enabled = false;
        Debug.Log("Dead!");
        yield return new WaitForSeconds(death);
        transform.position = gm.lastCheckPointPos;
        safe = true;
        coll.enabled = true;
        this.enabled = true;
        soundmanager.PlaySound("death");
        //GameOverScreen.SetActive(true);
    }
}
