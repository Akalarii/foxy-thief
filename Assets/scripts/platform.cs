﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class platform : MonoBehaviour
{
    private CompositeCollider2D comColl;
    public float startDisable = 0f;
    private float disable = 0.4f;
    private Collider2D coll;
    public Collider2D Player;
    private bool active = true;

    private void Start()
    {
        coll = GetComponent<TilemapCollider2D>();
        comColl = GetComponent<CompositeCollider2D>();
    }

    private void Update()
    {
        if (comColl.IsTouching(Player))
        {    
            Fall();
        }

        if (active == false)
        {
            startDisable += Time.deltaTime;
            if (startDisable >= disable)
            {
                startDisable = 0;
                Player.enabled = true;
                active = true;
            }
        }
    }
    private void Fall()
    {
        if (active)
        {
            if (Input.GetKey(KeyCode.S) && Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("hi");
                Player.enabled = false;
                active = false;
            }
        }
    }
}
