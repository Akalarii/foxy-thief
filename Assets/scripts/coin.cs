﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coin : MonoBehaviour
{
    public int coinValue = 1;
    public ParticleSystem particleSystem;
    public Animator animator;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ScoreManager.instance.ChangeScore(coinValue);
            animator.SetTrigger("Collect");
        }
    }
    
    public void PlaySound() {
        soundmanager.PlaySound("collect");
    }


    public void Remove()
    {
        
        Destroy(gameObject);
    }

    public void PlayParticles()
    {
        PlaySound();
        particleSystem.Play();
    }
}
