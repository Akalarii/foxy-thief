﻿using System;
using System.Collections;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class PlayerController : MonoBehaviour

{
    //----------------------------------------------------------------------------------------------------------------
    private CapsuleCollider2D coll;
    private Rigidbody2D rb;
    public Interaction interaction;
    private Animator anim;
    public GameObject GameOverScreen;
    
    private bool crouching = false;
    private bool playing = false;

    //----------------------------------------------------------------------------------------------------------------
    [Header("Combat: ")]
    public static int alertState;

    [Header("UI: ")]
    public GameObject camDefault;
    public GameObject camCrouch;

    [Header("Jump: ")] 
    public LayerMask Ground;
    public float lowJumpMultiplier = 2f;
    public float fallMultiplier = 2.5f;
    public float jumpForce = 5;
    public float antiForce = 5f;

    [Header("Movement: ")] 
    private Vector2 movement;
    public float moveSpeed = 5;
    public bool safe = true;
    private float velocity;
    
    public GameObject StairsA;
    public GameObject StairsB;
    public GameObject StairsC;
    public GameObject StairsD;

    public Collider2D StairsCollA;
    public Collider2D StairsCollB;
    public Collider2D StairsCollC;
    public Collider2D StairsCollD;
    //----------------------------------------------------------------------------------------------------------------
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        coll = GetComponent<CapsuleCollider2D>();
        anim = GetComponent<Animator>();

        StairsCollA = GetComponent<Collider2D>();
        StairsCollB = GetComponent<Collider2D>();
        StairsCollC = GetComponent<Collider2D>();
        StairsCollD = GetComponent<Collider2D>();
    }
    private void Update()
    {
        if (playing == false)
        {
            Crouch();
            if (crouching == false)
            {
                Jump();
            }
        }
        Death();
        Saxophone();
        //Stairs();
    }
    private void FixedUpdate()
    {
        if (playing == false)
        {
            Movement();
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Enemy"))
        {
            safe = false;
        }

        if (other.gameObject.tag.Equals("Light"))
        {
            safe = false;
        }
    }
    //----------------------------------------------------------------------------------------------------------------
    
    private void Death()
    {
        if (safe == false)
        {
            velocity = rb.velocity.x;
            StartCoroutine(TimeBeforeDeath());
        }
    }
    private void Movement()
    {
        if (interaction.hiding == false && crouching == false)
        {
            //Movement
            
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
            transform.position += moveSpeed * Time.deltaTime * movement;

            if (Mathf.Abs(movement.x) > 0)
            {
                anim.SetInteger("state", 1);
            }
            else
            {
                anim.SetInteger("state", 0);
            }

            //Flip Sprite
            if (movement.x > 0) rb.transform.localScale = new Vector3(1, 1, 1); 
            if (movement.x < 0) rb.transform.localScale = new Vector3(-1, 1, 1);
        
            //AntiForce in air.
            if (rb.velocity.y >= 0)
            {
                if (rb.velocity.x >= 5) rb.AddForce(Time.deltaTime * antiForce * Vector2.left);
                if (rb.velocity.x <= -5) rb.AddForce(Time.deltaTime * antiForce * Vector2.right);
            }
        }
    }
    private void Jump()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            crouching = false;
            rb.velocity = Vector2.up * jumpForce;
            //alertState = 1;
            StartCoroutine(alertCD(0.5f));
        }

        if (rb.velocity.y > 0)
        {
            if (rb.velocity.y < 0)
                rb.velocity += Time.deltaTime * Physics2D.gravity.y * (fallMultiplier - 1) * Vector2.up;
        }
    }
    private bool IsGrounded()
    {
        float extraHeight = 0.3f;
        var bounds = coll.bounds;
        RaycastHit2D raycast = Physics2D.Raycast(bounds.center, Vector2.down, bounds.extents.y + extraHeight, Ground);
        Debug.DrawRay(coll.bounds.center, Vector2.down * (bounds.extents.y + extraHeight), Color.red);
        return raycast.collider != null;
    }
    
    private void Crouch()
    {
        if (Input.GetKey(KeyCode.S) && IsGrounded())
        {
            if (crouching == false)
            {
                //--CROUCH--
                anim.SetInteger("state",3);
                crouching = true;                                                    
                coll.size = new Vector2(0.45f, 0.7f);                                          
                coll.offset = new Vector2(0.05f, -0.5f);         

                StartCoroutine(WaitTillCamMove());
            }
        }
        else
        {
            crouching = false;
            //anim.SetInteger("state", 0);
            coll.size = new Vector2(0.45f, 1.7f);
            coll.offset = new Vector2(0.05f, 0);
                
            camDefault.SetActive(true);
            camCrouch.SetActive(false);
        }
    }
    private void Saxophone()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Debug.Log("Insert nice Jazz-Sounds here");
            playing = true;
            alertState = 1;
            anim.SetInteger("state", 2);
            StartCoroutine(playSaxophone());
        }
    }
    //----------------------------------------------------------------------------------------------------------------
    
    private IEnumerator alertCD(float alertCD)
    {
        yield return new WaitForSeconds(alertCD);
        alertState = 0;
    }
    private IEnumerator TimeBeforeDeath()
    {
        this.enabled = false;
        rb.velocity = new Vector2(velocity, rb.velocity.y);
        yield return new WaitForSeconds(2);
        GameOverScreen.SetActive(true);
    }
    private IEnumerator WaitTillCamMove()
    {
        yield return new WaitForSeconds(3);
        camDefault.SetActive(false);
        camCrouch.SetActive(true);
    }
    private IEnumerator playSaxophone()
    {
        yield return new WaitForSeconds(1.5f);
        anim.SetInteger("state", 0);
        playing = false;
        alertState = 0;
    }
}