﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPos : MonoBehaviour
{
    private GameMaster gm;
    private GameObject mainCamera;

    private void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
           //mainCamera.SetActive(false);
            transform.position = gm.lastCheckPointPos;
        }
    }

    private IEnumerator Camera()
    {
        yield return new WaitForSeconds(0.5f);
    }
}

