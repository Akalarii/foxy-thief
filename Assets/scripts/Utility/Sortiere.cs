﻿using System;
using UnityEngine;

namespace Utility
{
    [ExecuteInEditMode]
    public class Sortiere : MonoBehaviour
    {
        public bool active;
        public GameObject swap;
        public new String tag;
        public Transform parent;
        
        private void Start()
        {
            if (active)
            {
                GameObject[] objects = GameObject.FindGameObjectsWithTag(tag);
                foreach (GameObject objecte in objects)
                {
                    Instantiate(swap, objecte.transform.position, Quaternion.identity, parent);
                    DestroyImmediate(objecte);
                }
            }   
        }   
            
    }
}