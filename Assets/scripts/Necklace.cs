﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Necklace : MonoBehaviour
{
    public int necklaceValue = 10;
    public ParticleSystem particleSystem;
    public Animator animator;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ScoreManager.instance.ChangeScore(necklaceValue);
            animator.SetTrigger("Collect");
        }
    }
    
    public void PlaySound() {
        soundmanager.PlaySound("collect");
    }


    public void Remove()
    {
        Destroy(gameObject);
    }

    public void PlayParticles()
    {
        PlaySound();
        particleSystem.Play();
    }
}