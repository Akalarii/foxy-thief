﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statue : MonoBehaviour
{
    public int statueValue = 20;
    public ParticleSystem particleSystem;
    public Animator animator;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ScoreManager.instance.ChangeScore(statueValue);
            animator.SetTrigger("Collect");
        }
    }
    
    public void PlaySound() {
        soundmanager.PlaySound("collect");
    }


    public void Remove()
    {
        
        Destroy(gameObject);
    }

    public void PlayParticles()
    {
        PlaySound();
        particleSystem.Play();
    }
}

