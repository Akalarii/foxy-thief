﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.UIElements;

public class SecurityCamera : MonoBehaviour
{
    private bool rotating = true;
    private float currentDelay = 0f;
    private GameObject player;

    //----------------------------------------------------------------------------------------------------------------

    [Header("Reference: ")]
    public Transform rayShooter;

    public int steps;
    public float angle = 20f;
    public float distance = 10f;
    public bool hasCaps;

    [Header("Movement: ")] 
    public float rightCap = 200f;
    public float leftCap = 150f;
    public float delay = 5f;
    public float rotationSpeed;

    private bool siren = true;

    //----------------------------------------------------------------------------------------------------------------
    void OnValidate()
    {
        Light2D light = GetComponent<Light2D>();
        
    }

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }
    
    private void Update()
    {
        if(rotating)
            Rotation();
        else
        {
            currentDelay += Time.deltaTime;
            if (currentDelay >= delay)
                rotating = true;
        }
        Detection();
    }
    
    //----------------------------------------------------------------------------------------------------------------

    private void Rotation()
    {
        transform.Rotate(0f,0f,rotationSpeed * Time.deltaTime);
        if (hasCaps)
        {
            if (transform.rotation.eulerAngles.z < leftCap)
            {
                transform.rotation = Quaternion.Euler(0f, 0f, leftCap);
                ChangeDirection();
            }
            else if (transform.rotation.eulerAngles.z > rightCap)
            {
                transform.rotation = Quaternion.Euler(0f, 0f, rightCap);
                ChangeDirection();
            }
        }
    }
    private void Detection()
    {
        for (int sample = 0; sample <= steps; sample++)
        {
            rayShooter.localRotation = Quaternion.Euler(0f,0f,-angle / 2);
            rayShooter.Rotate(0, 0, sample * angle / steps);
            RaycastHit2D hitInfo = Physics2D.Raycast(rayShooter.position, rayShooter.up, distance, ~18);
            if (hitInfo && hitInfo.collider.gameObject.CompareTag("Player"))
            {
                Player.safe = false;
                if (siren)
                {
                    soundmanager.PlaySound("alarm");
                    siren = false;
                    StartCoroutine(alarm());
                }
            }
            Debug.DrawRay(rayShooter.position, rayShooter.up * distance, Color.red);
        }
        /*var position = transform.position;
        foreach (var light in maxAngle)
        {
            RaycastHit2D hitInfo1 = Physics2D.Raycast(position, light.transform.position - position, 100);
            if (hitInfo1 && hitInfo1.collider.gameObject.layer == 10) PC.safe = false;
            //Debug.DrawRay(position, light.transform.position - position, Color.red);
        }*/
    }
    private void ChangeDirection()
    {
        rotationSpeed*= -1;
        rotating = false;
        currentDelay = 0f;
    }

    private IEnumerator alarm()
    {
        yield return new WaitForSeconds(3);
        siren = true;
    }

    //----------------------------------------------------------------------------------------------------------------
}