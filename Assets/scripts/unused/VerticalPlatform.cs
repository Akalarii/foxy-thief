﻿using UnityEngine;

public class VerticalPlatform : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.S))
        {
            GetComponent<EdgeCollider2D>().enabled = true;
        }

        if (Input.GetKey(KeyCode.S) && Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<EdgeCollider2D>().enabled = false;
        }
    }
}