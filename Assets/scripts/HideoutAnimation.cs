﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideoutAnimation : MonoBehaviour
{
    public bool hiding = false;
    private bool touching;
    public Animator anim;
    private Vector2 origin;
    private GameObject hideout;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(hiding)
            anim.SetInteger("State", 2);
        else if (touching)
            anim.SetInteger("State", 1);
        else
            anim.SetInteger("State", 0);
    }


    void OnTriggerStay2D(Collider2D other)
    {
        var playerRend = other.GetComponent<SpriteRenderer>();
        var playerRb = other.GetComponent<Rigidbody2D>();

        if (other.CompareTag("Player") || other.CompareTag("PlayerHiding"))
        {
            touching = true;
            anim.SetInteger("State", 1);
            if (Input.GetKeyDown(KeyCode.W))
            {
                if (hiding)
                {
                    playerRend.enabled = true;
                    playerRb.transform.position = origin;
                    hiding = false;
                    other.gameObject.tag = "Player";
                    other.GetComponent<Player>().enabled = true;
                }
                else
                {
                    playerRend.enabled = false;
                    playerRb.transform.localPosition =
                        new Vector3(transform.localPosition.x, playerRb.transform.position.y, 0);
                    origin = playerRb.transform.position;
                    other.gameObject.tag = "PlayerHiding";
                    hiding = true;
                    other.GetComponent<Player>().enabled = false;
                    
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
            touching = false;
    }
}