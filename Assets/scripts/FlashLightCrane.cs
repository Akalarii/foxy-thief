﻿using System;
using System.Collections;
using UnityEngine;

public class FlashLightCrane : MonoBehaviour
{
    public GameObject[] maxAngle;
    public float rayDistance;
    private bool soundplayed = true;

    private Crane crane;

    private void Start()
    {
        crane = GetComponentInParent<Crane>();
    }

    private void Update()
    {
        Detection();
    }

    private void Detection()
    {
        var position = transform.position;

        foreach (var angle in maxAngle)
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(position, (angle.transform.position - position), rayDistance, ~18);

            if (hitInfo && hitInfo.collider.gameObject.CompareTag("Player"))
            {
                Player.safe = false;
                StartCoroutine(Spotted());

                if (soundplayed)
                {
                    soundmanager.PlaySound("birdAlarm");
                    soundplayed = false;
                    StartCoroutine(sound());
                }
            }
        }
    }

    private IEnumerator sound()
    {
        yield return new WaitForSeconds(Player.death + 1);
        soundplayed = true;
    }
    private IEnumerator Spotted()
    {
        crane.stationary = true;
        yield return new WaitForSeconds(Player.death);
        crane.stationary = false;
    }
}