﻿using UnityEngine;

public class pointLight : MonoBehaviour

{
    private Vector2 direction;
    private RaycastHit2D hit2D;
    private Vector2 start;
    private readonly int steps = 8;
    private readonly float xStart = -56.8f;
    private float xWert = -56.8f;
    private readonly float yStart = -41.3f;
    private float yWert = -41.3f;

    public void Start()
    {
        start = new Vector2(transform.position.x, transform.position.y);
    }

    private void Update()
    {
        RaycastStart();
    }

    private void RaycastStart()
    {
        if (Input.GetKey(KeyCode.R))
        {
            xWert = xStart;
            yWert = yStart;
            direction = new Vector2(xWert, yWert);

            for (int i = 0; i <= steps; i++)
            {
                //Debug.Log(i + " / " + direction);
                //Debug.DrawRay(start, direction, Color.magenta,1000f);
                RaycastHit2D hit2D = Physics2D.Raycast(start, direction, Mathf.Infinity);
                if (hit2D.collider)
                    Debug.Log(hit2D.collider.name);
                else
                    Debug.Log("Nothing hit!");
                direction += new Vector2(4, -4);
            }
        }
    }
}