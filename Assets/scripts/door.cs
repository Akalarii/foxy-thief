﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class door : MonoBehaviour
{
    // Start is called before the first frame update
    private bool interactive = false;
    private GameObject player;
    private Animator anim;
    
    public Transform targetPosition;
    

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            interactive = true;
            anim.SetInteger("state", 1);
            player = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            interactive = false;
            player = null;
            anim.SetInteger("state", 0);
        }
    }

    private void Update()
    {
        if (interactive && Input.GetKeyDown(KeyCode.W))
        {
            if (player)
            {
                player.GetComponent<Rigidbody2D>().position = targetPosition.position;
            }
        }
    }
}